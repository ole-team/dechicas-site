<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package cenote
 */

?>
		</div><!-- .tg-container -->
	</div><!-- #content -->

	<?php
	// Show related post if enabled.
	if ( true === get_theme_mod( 'cenote_single_enable_related_post', true ) && is_single() ) {
		get_template_part( 'template-parts/related/related', 'post' );
	}
	?>
	<footer id="colophon" class="site-footer tg-site-footer <?php cenote_footer_class(); ?>">
		<div class="tg-footer-top">
			<div class="tg-container">
				<?php //get_sidebar( 'footer' ); ?>
			</div>
		</div><!-- .tg-footer-top -->

		<div class="tg-footer-bottom">
			<div class="tg-container">
				<div class="tg-footer-bottom-container tg-flex-container">
					<div class="tg-footer-bottom-left">
						<?php get_template_part( 'template-parts/footer/footer', 'info' ); ?>
					</div><!-- .tg-footer-bottom-left -->
					<div class="tg-footer-bottom-right">
					</div><!-- .tg-footer-bottom-right-->
				</div><!-- .tg-footer-bootom-container-->
			</div>
		</div><!-- .tg-footer-bottom -->
	</footer><!-- #colophon -->

</div><!-- #page -->
<?php
do_action( 'cenote_after_footer' );
wp_footer();
?>
<?php if(has_tag('branded')){
    //avoid Ads
}else{ ?>
<div id="wrap_footer_banner" style="position:fixed; bottom:0px; width:fit-content; z-index:9999; left: 50%; transform: translateX(-50%); text-align:center; display: none;">
    <div id='footbanner'>
        <script>
            googletag.cmd.push(function() { googletag.display('footbanner'); });
        </script>
    </div>
    <div id="close_banner_ft" style="position: absolute; border-radius: 25px; background-color: #000; color: #fff; width: 25px; height: 25px; font-size: 20px; text-align: center;  top: -10px; left: 0px;  cursor: pointer;">x</div>
    </div>
<div id="overlay_1x1">
    <script>
    googletag.cmd.push(function() { googletag.display("overlay_1x1"); });
    </script>
</div>
<?php } ?>
<?php if ( is_single() ) { ?>
<div id="intext_1x1">
    <script>
    googletag.cmd.push(function() { googletag.display("intext_1x1"); });
    </script>
</div>
<script>
jQuery(document).ready(function($){
    var itemvideod = $('div[id*=-DLM]');
    itemvideod.each(function(i){
        var idDM = $(this).attr('data-id')
        var elementPlayer = $(this).attr('id');
        var videoCtitle = $(this).attr('data-title');
        var controlPlayer = 0;
        if(idDM){
            window['playerContent'+i] = DM.player(document.getElementById(elementPlayer), {
                video: idDM, 
                width: "100%", 
                height: "100%", 
                params: { 
                    autoplay: true, 
                    mute: true, 
                    "queue-enable": false, 
                    "queue-autoplay-next": false, 
                    "sharing-enable":false
                } 
            }); 
        }
    });
    number_paragraphs = $('.entry-content').find('p').length;
    distribuiteP = Math.round(number_paragraphs / 2);
    <?php if(!has_tag('branded')){ ?>
    $.each($('.entry-content p'), function (index, paragraph) {
        if(distribuiteP >= 2){
            var controlp = distribuiteP;
            if (index == controlp) {
              $(paragraph).before('<div class="own-ads"><div id="midbanner" style="text-align:center; margin-left: -10px;"></div></div>');
              googletag.cmd.push(function() { googletag.display("midbanner"); });
            }
        }else{
            if (index == 3) { 
              $(paragraph).after('<div class="own-ads"><div id="midbanner" style="text-align:center; margin-left: -10px;"></div></div>');
              googletag.cmd.push(function() { googletag.display("midbanner"); });
            }
        }
    });
    <?php } ?>
});
</script>
<?php } ?>
<script type="text/javascript">
    jQuery(document).ready(function($){
        $('#close_banner_ft').on('click',function(){
            $('#wrap_footer_banner').remove();
        });
    });
</script>
</body>
</html>
