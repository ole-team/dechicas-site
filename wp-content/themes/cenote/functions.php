<?php
/**
 * Cenote functions and definitions
 *
 * @link    https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package cenote
 */

if ( ! function_exists( 'cenote_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function cenote_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on cenote, use a find and replace
		 * to change 'cenote' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'cenote', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		/**
		 * Enable support for post formats
		 *
		 * @link https://developer.wordpress.org/themes/functionality/post-formats/
		 */
		add_theme_support( 'post-formats', array( 'gallery', 'link', 'image', 'quote', 'video', 'audio' ) );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'tg-menu-primary'    => esc_html__( 'Primary', 'cenote' ),
				'tg-menu-header-top' => esc_html__( 'Header Top', 'cenote' ),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
			)
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'cenote_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);

		add_editor_style();

		// Gutenberg wide layout support.
		add_theme_support( 'align-wide' );

		// Gutenberg block layout support.
		add_theme_support( 'wp-block-styles' );

		// Gutenberg editor support.
		add_theme_support( 'responsive-embeds' );


	}
endif;
add_action( 'after_setup_theme', 'cenote_setup' );

/**
 * Custom image sizes for theme.
 */
function cenote_image_sizes() {

	// Set default post thumbnail. 16:9.
	set_post_thumbnail_size( 768, 432, true );
	// Large full width image.
	add_image_size( 'cenote-full-width', 1160, 653, true );
	// 3:4.
	add_image_size( 'cenote-post', 600, 400, true );
	// Auto size.
	add_image_size( 'cenote-post-auto', 600, 9999, false );

}

add_action( 'after_setup_theme', 'cenote_image_sizes' );

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 770;
}

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Set the content width as selected on the theme options.
 *
 * @global int $content_width
 */
function cenote_content_width() {
	global $post, $content_width;

	if ( $post ) {
		$layout = get_post_meta( $post->ID, 'cenote_post_layout', true );
	}

	if ( empty( $layout ) ) {
		$layout = 'layout--default';
	}

	$single_post_page_layout = get_theme_mod( 'cenote_layout_single', 'layout--right-sidebar' );
	$single_page_layout      = get_theme_mod( 'cenote_layout_page', 'layout--right-sidebar' );

	if ( 'layout--default' === $layout ) {
		if ( ( ( 'layout--no-sidebar' === $single_post_page_layout ) && is_single() ) || ( ( 'layout--no-sidebar' === $single_page_layout ) && is_page() ) ) {
			$content_width = 1160;
		} else {
			$content_width = 770;
		}
	} elseif ( 'layout--no-sidebar' === $layout ) {
		$content_width = 1160;
	} else {
		$content_width = 770;
	}
}

add_action( 'template_redirect', 'cenote_content_width' );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function cenote_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'cenote' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'cenote' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);

	register_sidebar(
		array(
			'name'          => esc_html__( 'Footer 1', 'cenote' ),
			'id'            => 'footer-sidebar-1',
			'description'   => esc_html__( 'Add widgets here to show on footer 1.', 'cenote' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);

	register_sidebar(
		array(
			'name'          => esc_html__( 'Footer 2', 'cenote' ),
			'id'            => 'footer-sidebar-2',
			'description'   => esc_html__( 'Add widgets here to show on footer 2.', 'cenote' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);

	register_sidebar(
		array(
			'name'          => esc_html__( 'Footer 3', 'cenote' ),
			'id'            => 'footer-sidebar-3',
			'description'   => esc_html__( 'Add widgets here to show on footer 3.', 'cenote' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);

	register_sidebar(
		array(
			'name'          => esc_html__( 'Footer 4', 'cenote' ),
			'id'            => 'footer-sidebar-4',
			'description'   => esc_html__( 'Add widgets here to show on footer 4.', 'cenote' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}

add_action( 'widgets_init', 'cenote_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function cenote_scripts() {
	$suffix = ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) ? '' : '.min';

	wp_enqueue_style( 'cenote-style', get_stylesheet_uri() );
	wp_style_add_data( 'cenote-style', 'rtl', 'replace' );

	wp_enqueue_style( 'themegrill-icons', get_template_directory_uri() . '/assets/css/themegrill-icons' . $suffix . '.css', array(), '1.0' );

	wp_enqueue_style( 'fontawesome', get_template_directory_uri() . '/assets/css/all' . $suffix . '.css' );

	wp_enqueue_script( 'cenote-skip-link-focus-fix', get_template_directory_uri() . '/assets/js/skip-link-focus-fix' . $suffix . '.js', array(), '20151215', true );
	wp_enqueue_script( 'hammer', get_template_directory_uri() . '/assets/js/hammer' . $suffix . '.js', array(), '2.0.8', true );

	// Load Swiper on Gallery post format.
	wp_enqueue_script( 'swiper', get_template_directory_uri() . '/assets/js/swiper' . $suffix . '.js', array(), '4.2.0', true );
	wp_enqueue_style( 'swiper', get_template_directory_uri() . '/assets/css/swiper' . $suffix . '.css', '4.2.0' );

	// Load headroom if sticky header is enabled.
	if ( true === get_theme_mod( 'cenote_header_sticky_option', true ) ) {
		wp_enqueue_script( 'headroom', get_template_directory_uri() . '/assets/js/Headroom' . $suffix . '.js', array(), '0.9.4', true );
	}

	// Only load masonry script if enabled.
	if ( ( is_archive() || is_home() ) && 'tg-archive-style--masonry' === get_theme_mod( 'cenote_archive_style', 'tg-archive-style--masonry' ) ) {
		wp_enqueue_script( 'masonry', get_template_directory_uri() . '/assets/js/masonry.pkgd' . $suffix . '.js', array(), '4.2.0', true );
		wp_enqueue_script( 'imagesloaded', get_template_directory_uri() . '/assets/js/imagesloaded.pkgd' . $suffix . '.js', array(), '4.1.4', true );
	}

	wp_enqueue_script( 'cenote-custom', get_template_directory_uri() . '/assets/js/cenote-custom' . $suffix . '.js', array(), '1.0.0', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}

add_action( 'wp_enqueue_scripts', 'cenote_scripts' );

/**
 * Enqueue Google fonts and editor styles.
 */
function cenote_block_editor_styles() {
	wp_enqueue_style( 'cenote-editor-googlefonts', '//fonts.googleapis.com/css2?family=Roboto' );
	wp_enqueue_style( 'cenote-block-editor-styles', get_template_directory_uri() . '/style-editor-block.css' );
}

add_action( 'enqueue_block_editor_assets', 'cenote_block_editor_styles', 1, 1 );


/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Meta boxes.
 */
require get_template_directory() . '/inc/meta-boxes.php';

/**
 * Widget for showing recent post
 */
require get_template_directory() . '/inc/widgets/class-cenote-widget-recent-posts.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/**
 * Load WooCommerce compatibility file.
 */
if ( class_exists( 'WooCommerce' ) ) {
	require get_template_directory() . '/inc/woocommerce.php';
}

/**
 * Add Kirki customizer library file
 */
require get_template_directory() . '/inc/kirki/kirki.php';

/**
 * Add Kirki options file
 */
require get_template_directory() . '/inc/customizer/kirki-customizer.php';

/**
 * Add theme css class control file
 */
require get_template_directory() . '/inc/template-css-class.php';

/**
 * Load breadcrumb trail file if enabled.
 */
if ( true === get_theme_mod( 'cenote_breadcrumb', true ) ) {
	require get_template_directory() . '/inc/compatibility/class-breadcrumb-trail.php';
}

/**
 * Load Demo Importer Configs.
 */
if ( class_exists( 'TG_Demo_Importer' ) ) {
	require get_template_directory() . '/inc/demo-config.php';
}

/**
 * Assign the Cenote version to a variable.
 */
$cenote_theme = wp_get_theme( 'cenote' );

define( 'CENOTE_THEME_VERSION', $cenote_theme->get( 'Version' ) );

/**
 * Calling in the admin area for the new theme notice.
 */
if ( is_admin() ) {
	require get_template_directory() . '/inc/admin/class-cenote-admin.php';
	require get_template_directory() . '/inc/admin/class-cenote-notice.php';
	require get_template_directory() . '/inc/admin/class-cenote-theme-review-notice.php';
	require get_template_directory() . '/inc/admin/class-cenote-upgrade-notice.php';
	require get_template_directory() . '/inc/admin/class-cenote-dashboard.php';
	require get_template_directory() . '/inc/admin/class-cenote-welcome-notice.php';
}


/* Here we go with the custom settings by OleDigital */

add_filter( 'the_content', 'prefix_insert_post_ads' );
 
function prefix_insert_post_ads( $content ) {
    $ad_code = '<div class="ads-left"><div id="sqrbanner1"><script>googletag.cmd.push(function() { googletag.display("sqrbanner1"); });</script></div></div>';
    if ( is_single() ) {
        $tempcontent = $ad_code.$content;
    }else{
    	$tempcontent = $content;
    }
    return  $tempcontent;
}

function get_post_primary_category( $post = 0, $taxonomy = 'category' ){
    if ( ! $post ) {
        $post = get_the_ID();
    }

    $terms        = get_the_terms( $post, $taxonomy );
    $primary_term = array();

    if ( $terms ) {
        $term_display = '';
        $term_slug    = '';
        $term_link    = '';
        if ( class_exists( 'WPSEO_Primary_Term' ) ) {
            $wpseo_primary_term = new WPSEO_Primary_Term( $taxonomy, $post );
            $wpseo_primary_term = $wpseo_primary_term->get_primary_term();
            $term               = get_term( $wpseo_primary_term );
            if ( is_wp_error( $term ) ) {
                $term_display = $terms[0]->name;
                $term_slug    = $terms[0]->slug;
                $term_link    = get_term_link( $terms[0]->term_id );
            } else {
                $term_display = $term->name;
                $term_slug    = $term->slug;
                $term_link    = get_term_link( $term->term_id );
            }
        } else {
            $term_display = $terms[0]->name;
            $term_slug    = $terms[0]->slug;
            $term_link    = get_term_link( $terms[0]->term_id );
        }
        $primary_term['url']   = $term_link;
        $primary_term['slug']  = $term_slug;
        $primary_term['title'] = $term_display;
    }
    return $primary_term;
}
use Facebook\InstantArticles\Elements\Ad;
add_filter('instant_articles_content','custom_ad_content',10,1);
function custom_ad_content($content){
    $id = get_the_id();
    $vdjsd = get_post_meta( $id, 'video_destacado', true );
    $vdjsr = get_post_meta( $id, 'video_recomendado', true );
    if(!empty($vdjsd)){
        $videjod = '<iframe frameborder="0" width="300" height="300" src="https://www.dailymotion.com/embed/video/'.$vdjsd.'?ads_params=site%3Di24_rs&sharing-enable=false&queue-enable=false&queue-autoplay-next=false&quality=380&autoplay=true&mute=true"></iframe>';
        $content = $videjod.$content;
    }
    if(!empty($vdjsr)){
        $videjor = '<iframe frameborder="0" width="300" height="300" src="https://www.dailymotion.com/embed/video/'.$vdjsr.'?ads_params=site%3Di24_rs&sharing-enable=false&queue-enable=false&queue-autoplay-next=false&quality=380&autoplay=true&mute=true"></iframe>';
        $content = $content.$videjor;
    }
    return $content;
}
add_shortcode('videoDLM', function($atts){
    $html = '';
    $idDLM = $atts['id'];
    $typeDLM = $atts['type'];
    if(!empty($idDLM)){
        $html .= '<div class="video_wrapper_responsive vd-'.$typeDLM.'"><div id="'.$typeDLM.'-DLM" data-id="'.$idDLM.'"></div></div>';
    wp_enqueue_script('DMapi', 'https://api.dmcdn.net/all.js');
    }  
    return $html;
});
//Allow disable Facebook Instant Article

add_filter("instant_articles_should_submit_post", "ad_validation_FBIA",10, 2);
function ad_validation_FBIA($should_show, $IA_object){
    $validateIA = get_post_meta($IA_object->get_the_id(), "disable_ia");
    if($validateIA){
        return false;
    }else{
        return true;
    }   
}
add_action('post_submitbox_misc_actions', 'create_disable_ia');
add_action('save_post', 'save_disable_ia');
function create_disable_ia(){
    $post_id = get_the_ID();
  
    if (get_post_type($post_id) != 'post') {
        return;
    }
  
    $value = get_post_meta($post_id, 'disable_ia', true);
    wp_nonce_field('ad_disable_ia_nonce_'.$post_id, 'ad_disable_ia_nonce');
    ?>
    <div class="misc-pub-section misc-pub-section-last">
        <label><input type="checkbox" value="1" <?php checked($value, true, true); ?> name="disable_ia" /><?php _e('Disable this post in FB-IA', 'pmg'); ?></label>
    </div>
    <?php
}
function save_disable_ia($post_id){
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
        return;
    }
    
    if (
        !isset($_POST['ad_disable_ia_nonce']) ||
        !wp_verify_nonce($_POST['ad_disable_ia_nonce'], 'ad_disable_ia_nonce_'.$post_id)
    ) {
        return;
    }
    
    if (!current_user_can('edit_post', $post_id)) {
        return;
    }
    if (isset($_POST['disable_ia'])) {
        update_post_meta($post_id, 'disable_ia', $_POST['disable_ia']);
    } else {
        delete_post_meta($post_id, 'disable_ia');
    }
}
function user_can_richedit_custom() {
    global $wp_rich_edit;
    if (get_user_option('rich_editing') == 'true' || !is_user_logged_in()) {
        $wp_rich_edit = true;
        return true;
    }
    $wp_rich_edit = false;
    return false;
}

add_filter('user_can_richedit', 'user_can_richedit_custom');
function videos_content( $content ) {
    if ( is_single() && 'post' == get_post_type() ) {

        $custom_content = "";

        $custom_content .= $content;
        $live = false;
        $video_DLM = get_post_meta( get_the_ID(), 'video_recomendado', true );
        
        $post_tags = get_the_tags( get_the_ID());
        if ( function_exists( 'ampforwp_is_amp_endpoint' ) && ampforwp_is_amp_endpoint() ) {
            if(!empty($v_DLM)){
                $precontent = '<amp-dailymotion data-videoid="'.$v_DLM.'" autoplay data-mute="true" layout="responsive" width="480" height="270"></amp-dailymotion></br>';
                $custom_content = $precontent.$content;
            }
            if(!empty($video_DLM)){
                $custom_content .= '<strong>Imperdible</strong><br />';
                $custom_content .= '<amp-dailymotion data-videoid="'.$video_DLM.'" autoplay data-mute="true" layout="responsive" width="480" height="270"></amp-dailymotion>';
                
            }
        }else{
            if( has_tag( 'branded' ) ) {
            }else{
            if(!empty($video_DLM)){
                $custom_content .= '<div class="group_video_r"><strong>Imperdible</strong><br />';
                $custom_content .= do_shortcode('[videoDLM id="'.$video_DLM.'" type="recomendado" title="'.$tt_recomendado.'"]');
                $custom_content .= '</div>';
                }
            }
        }
        return $custom_content;
    } else {
        return $content;
    }
}
add_filter( 'the_content', 'videos_content' );

add_action( 'init', function(){
    wp_embed_register_handler( 
        'myig', 
        '/https?\:\/\/(?:www.)?instagram.com\/p\/(.+)/',   // <-- Adjust this to your needs!
        'ig_embed_handler' 
    );
} );
//clean ig embed
function ig_embed_handler( $matches, $attr, $url, $rawattr ){
    $idIG = $matches[1];
    if(!empty($idIG)){
        if(strpos($idIG, 'photo') !== false){
            $idIG = str_replace('photo/', '', $idIG);
            $baseurl = 'https://graph.facebook.com/v10.0/instagram_oembed?url=instagram.com/p/'.$idIG.'&fields=thumbnail_url,author_name&access_token=757874017927860|9eae7061d9a8d7177a64e84ff2a45bb2';
            $dataDIG = file_get_contents($baseurl);
            $igEmbed = json_decode($dataDIG,true);
            $imgIG = $igEmbed["thumbnail_url"];
            $authorIG = $igEmbed["author_name"];
            if(!empty($imgIG)){
                $html = '<figure style="width: 100%" class="wp-caption"><a href="https://www.instagram.com/p/'.$idIG.'"><img src="'.$imgIG.'" alt=""></a><figcaption class="wp-caption-text"><a href="https://www.instagram.com/'.$authorIG.'", target="_blank" rel="noopener">Fuente: instagram/'.$authorIG.' </a></figcaption></figure>';
                return $html;
            }
        }else{
            $baseurl = 'https://graph.facebook.com/v10.0/instagram_oembed?url=instagram.com/p/'.$idIG.'&fields=html&access_token=446337026596225|6ecffb63a90e3fa01434225706abd2fd';
            $dataDIG = file_get_contents($baseurl);
            $igEmbed = json_decode($dataDIG,true);
            $embedIG = $igEmbed["html"];
            if(!empty($embedIG)){
                $newhtml = strip_tags($embedIG, '<blockquote><div><figure><p><a><script>');
                $newhtml = '<div class="embed">'.$newhtml.'</div>';
                return $newhtml;
            }
        }
    }
    
}

function appAD_fetch_embed( $cached_html, $url, $attr, $post_id ) {
    if ( false !== strpos( $url, "://twitter.com")){
        $pattern  = '#https?://twitter\.com/(?:\#!/)?(\w+)/status(es)?/(\d+)#is';
        preg_match( $pattern, $url, $matches );
        $urlt = $matches[0];
        $urltw = 'https://twitframe.com/show?url='.$urlt;
        $cached_html = '<div class="embed" style="max-width:500px;"><iframe border="0" frameborder="0" height="410" width="100%" src="'.$urltw.'"></iframe></div>';
    }
    return $cached_html;
}
add_filter('embed_oembed_html','appAD_fetch_embed', 10, 4 );

function custom_embed_settings($code){
    if(strpos($code, 'dai.ly') !== false || strpos($code, 'dailymotion.com') !== false){
        $return = preg_replace("@src=(['\"])?([^'\">\s]*)@", "src=$1$2?queue-autoplay-next=false&queue-enable=false&sharing-enable=false", $code);
        return '<div class="video_wrapper_responsive embed">'.$return.'</div>';
    }
    if(strpos($code, 'tiktok.com') !== false){
        return '<div class="embed">'.$code.'</div>';
    }
    return $code;

}

add_filter('embed_handler_html', 'custom_embed_settings');
add_filter('embed_oembed_html', 'custom_embed_settings');

function add_meta_robots_tags($robots) {
    if(is_single()){
        if(has_tag('exclude')){
            return 'noindex, nofollow';
        }
    }
    return $robots;
}
add_filter("wpseo_robots", 'add_meta_robots_tags');
add_action('wp_head', 'adOPs_calls',10);
function adOPs_calls(){
    $adTagCall = '';
    $typep = '';
    $varcontrol = 1;
    if(is_single()){
        if(has_tag('noads') or has_tag('branded')){
            $varcontrol  = 0;
        }
        $tempID = get_the_ID();
        $prm_ct = get_post_primary_category($tempID, 'category');
        if(!empty($prm_ct)){
            $temp_section = $prm_ct['slug'];
            $section = $temp_section;

        }
        $typep = 'nodes';
    }
    if(is_page()){
        $varcontrol = 1;
        $typep = 'page';
    }
    if($varcontrol == 1){
        $tags = '<!--'.$varcontrol.'--><script>';
        $tags .= '(function($) {
        var sizesScroll = [[728, 90],[970, 90]];
        var sizesTop = [[728, 90],[970, 90]];
        var sizesMid = [[728, 90],[728,250]];
        var sizesSqr1 = [300,250];
        var sizesSqr6 = [[728, 90]];
        var sizesFoot = [[728, 90],[970, 90],[1,1]];
        if ($(window).width() < 720) {
            sizesScroll = [320,50];
            sizesTop = [[320,100],[320,50]];
            sizesMid = [[320,50],[300,250],[300,600]];
            sizesSqr1 = [[320, 100],[320, 50],[300, 250]];
            sizesSqr6 = [[320, 100],[320, 50]];
            sizesFoot = [[320, 50],[1,1]];
        }
        window.googletag = window.googletag || {cmd: []};
        googletag.cmd.push(function() { '; 
        $tags .= 'googletag.defineSlot("/4923229/scrollbanner_mid_728x90_970x90_300x250_320x50", sizesScroll, "scrollbanner").addService(googletag.pubads());
        googletag.defineSlot("/4923229/top_banner_atf_728x90_970x90_320x50_320x100", sizesTop, "topbanner").addService(googletag.pubads());
        googletag.defineSlot("/4923229/midbanner_mid_320x50_300x250_336x280_728x90_728x250", sizesMid, "midbanner").addService(googletag.pubads());
        googletag.defineSlot("/4923229/sqrbanner1_atf_300x250_336x280_300x600", sizesSqr1, "sqrbanner1").addService(googletag.pubads());
        googletag.defineSlot("/4923229/sqrbanner6_mid_320x50_300x250_336x280_728x90_728x250", sizesSqr6, "sqrbanner6").addService(googletag.pubads());
        googletag.defineSlot("/4923229/footbanner_btf_728x250_728x90_970x90_300x250", sizesFoot, "footbanner").addService(googletag.pubads());
        googletag.defineSlot("/4923229/nativeintext_1x1",[1,1], "overlay_1x1").addService(googletag.pubads());';
        if(is_single()){
            $tags .= 'googletag.defineSlot("/4923229/native_intext_1x1",[1,1], "intext_1x1").addService(googletag.pubads());';
        }
        if(is_front_page()){
            $typep = 'home';    
        }
        if(is_category()){
            $catSite = get_the_category();
            if ( ! empty( $catSite ) ) {
                $temp_section  = $catSite[0]->slug; 
                $section = $temp_section;
            }
            $typep = 'category';    
        }
        $tags .= 'googletag.pubads().setTargeting("site", "dechicas");';
        if(is_front_page()){
            $tags .= 'googletag.pubads().setTargeting("tipo_de_pag", "home");';
        }else{
            $tags .= 'googletag.pubads().setTargeting("tipo_de_pag", "'.$typep.'");';
            $tags .= 'googletag.pubads().setTargeting("category", "'.$section.'");';
        }
        $tags .= 'googletag.pubads().collapseEmptyDivs();';
        $tags .= 'var SECONDS_TO_WAIT_AFTER_VIEWABILITY = 30;
        googletag.pubads().addEventListener("impressionViewable", function(event) {
            var slot = event.slot;
            if (slot.getTargeting("refresh").indexOf("true") > -1) {
            setTimeout(function() {
                googletag.pubads().refresh([slot]);
            }, SECONDS_TO_WAIT_AFTER_VIEWABILITY * 1000);
            }
        });
        googletag.pubads().addEventListener("slotRenderEnded", function(event) {
            if (event.slot.getSlotElementId() == "footbanner") {
                $("#wrap_footer_banner").fadeIn("fast");  
            }
        });';
        $tags .= 'googletag.enableServices(); }); })(jQuery);';
        $tags .= '</script>';
        echo $tags;
    }
}
function ad_remove_tags( $term_links ) {
    $result = array();
    $exclude_tags = array( 'exclude' );
    foreach ( $term_links as $link ) {
        foreach ( $exclude_tags as $tag ) {
            if ( stripos( $link, $tag ) !== false ) continue 2;
        }
        $result[] = $link;
    }
    return $result;
}
add_filter( "term_links-post_tag", 'ad_remove_tags', 100, 1 );
